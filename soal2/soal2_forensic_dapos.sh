mkdir forensic_log_website_daffainfo_log


awk 'BEGIN { FS= ":" }
{  }
END {
totalJam = 12
print "Rata-rata serangan adalah sebanyak ", NR/totalJam, " request per jam"}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt

awk 'BEGIN { 
FS = ":"
terbanyak = 0
print ""
}

{ 
if(NR>1) {ip[$1]++}

 }
END{
	for (i in ip){
#		print i, " has accessed ", ip[i], " times"
		if(ip[i] > terbanyak){
			terbanyak = ip[i]
		}
	}

printf "ip yang paling banyak mengakses server adalah:"
	for (i in ip){
		if( ip[i] == terbanyak){
			printf "%s sebanyak %d requests", i, ip[i]
		}
	}
print ""
}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt



awk '/curl/ { # print $0
i++}
END{
print ""
print "Ada ", i, "request yang menggunakan curl sebagai user-agent"
}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt



awk 'BEGIN { FS = ":"
print "" }

/22\/Jan\/2022:02:/{ ip[$1]++ }

END{
	for (i in ip) print i
}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
