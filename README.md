# soal-shift-sisop-modul-1-E13-2022



## Anggota Kelompok
Nama | NRP
---------- | ----------
Maheswara Danendra Satriananda | 5025201060i
Luthfiyyah hanifah amari | 5025201090
Mohammad Fany Faizul Akbar | 5025201225


## Penjelasan Penyelesaian Soal

### Soal 1

Dari soal no 1 dapat dipahami bahwa Han ingin meminjamkan laptopnya dengan cara membuat sebuah program yang memudahkan teman temannya, program tersebut memiliki fitur seperti register, login, download image, dan attempt check. 


a. `register.sh` 

Membuat direktori lokal serta mengatur waktu dan tanggal.
```
master=/home/mahes/Seasoup/users
Users=$master/user.txt
logs=$master/log.txt
calendar=$(date +%D)
time=$(date +%T)
```

Membuat fungsi yang digunakan untuk mengecek password dan username, serta jika belum ada local directory yang bernama $master maka akan membuat local directory tersebut terleih dahulu.
```
func_checkpassword() {
length=${#pword}

    if [[ ! -d "$master" ]]
    then
	mkdir $master
    fi
```

Fungsi checkpassword berisi beberapa kondisi yang menggunakan grep untuk menyamakan user yang di input dan user yang sudah ada di directory, dan menuliskan REGISTER:ERROR ke directory log.
```
    if grep -q $uname "$Users"
    then
	echo "User ini sudah ada bestie"
	echo "$calendar $time REGISTER:ERROR User already exists" >> $logs
```

Kondisi selanjutnya adalah beberapa kondisi untuk password, sesuai soal yang ada password harus memiliki panjang sebanyak 8 karakter, tidak boleh sama dengan username, dan minimal harus ada 1 uppercase dan 1 lowercase. Apabila password memenuhi kriteria tersebut semua maka username dan password akan ter-register dan tercatat pada directory serta akan menuliskan REGISTER:INFO ke directory log.
``` 
    elif [[ $pword == $uname ]]
    then
        echo "Password gaboleh sama seperti unameya bestie"
    elif [[ $length -lt 8 ]]
    then
        echo "Password harus lebih dari 8 karakter dongg"
    elif [[ "$pword" != *[[:upper:]]* || "$pword" != *[[:lower:]]* ]]
    then
	echo "Passwordnya minim ada 1 uppercase dan 1 lowercase ya bestie"
    else
      	echo "Register successfull!"
      	echo "$calendar $time REGISTER:INFO User $uname registered successfully" >> $logs
      	echo $uname $pword >> $Users
    fi
```

Meng-input username dan password lalu memanggil fungsi checkpassword.
```
printf "Username: "
read uname

printf "Password: "
read -s pword

func_checkpassword
```


b. `main.sh`

Membuat direktori lokal.
```
calendar=$(date +%D)
time=$(date +%T)
Log=/home/mahes/Seasoup/log/log.txt
User=/home/mahes/Seasoup/users/user.txt
```

Meng-insert username dan password yang akan digunakan untuk login lalu memanggil fungsi login.
```
echo "Hai login dlu ya!!"
printf "Username: "
read uname

echo "Password: "
read -s pword

folder=$(date +%Y-%m-%d)_$uname

func_login
```

Membuat function yang menjalankan operasi login menggunakan grep untuk menyamakan password dan username dari input dan dari directory $User.
```
func_login(){
	if [[ -n "$(grep $uname "$User")" ]] && [[ -n "$(grep $pword "$User")" ]]
		then
```

Setelah berhasil login, sistem secara otomatis akan menuliskan LOGIN:INFO User ke directory $log dan juga meng-echo kan kata kata "You are logged in".
```
echo "$calendar $time LOGIN: INFO User $uname logged in" >> $Log
			echo "You are logged in"
```

Jika prosess login sudah selesai maka selanjutnya sistem akan memberikan pilihan command bagi user, Command ini terdiri dari att dan dl dimana att adalah command untuk mengetahui berapa banyak attempt untuk login dari username yang sedang digunakan sedangkan dl adalah command untuk mendownload gambar dari link `https://loremflickr.com/320/240`.
```
printf "Mau ngaps gan? [dl // att]: "
			read command
			if [[ $command == att ]]
			then
				func_att
			elif [[ $command == dl ]]
			then
				func_dl
			else
				echo "Not found"
			fi
```

Apabila usaha login yang di awal ternyata gagal maka akan mengeluarkan output "wrong password" serta akan menuliskan LOGIN:ERROR ke directory $log, dan apabila user tersebut belum ada pada directory $User maka akan mengeluarkan output "User ini belum terdaftar"
```
elif [[ -n "$(grep $uname "$User")" ]]
			then
			echo "Wrong password"
			echo "$calendar $time LOGIN: ERROR Failed login attempt on user $uname " >> $Log
		else
			echo "User ini belum terdaftar"	
		fi
```

Pada func dl ini user akan disuruh meng insertkan berapa banyak foto yang ingin di download. Jika tidak ada folder yang bernama $folder.zip maka akan membuat file bernama $folder dan memanggil func start, dan apabila sudah ada maka akan memanggil func unzip.
```
func_dl(){
	printf "Mau berapa kali? : "
	read n

	if [[ ! -f "$folder.zip" ]]
	then
		mkdir $folder
		func_start
	else
		func_unzip
	fi
}
```

Pada func start akan dilakukan looping berdasarkan input jumlah dari user dan melakukan wget dari link yang diberikan, setelah itu dilakukan zipping pada folder yang ada.
```
func_start(){
	for(( i=1; i<=n; i++ ))
	do
		wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
	done

	zip --password $pword -r $folder.zip $folder/
	rm -rf $folder
}
```

Jadi function ini basically akan meng-unzip file yang sudah ada
```
func_unzip(){
	unzip -P $pword $folder.zip
	rm $folder.zip
```

Func att akan melakukan perhitungan pada directory $log, jika file $log belum ada maka akan meng output "Belum ada log" namun jika ada maka akan menjalankan fungsi awk.
```
func_att(){
	if [[ ! -f "$Log" ]]
	then
		echo "Belum ada log"
	else
		awk -v user="$uname" 'BEGIN {logged=0} $6 == user {logged++} 
		END {print (logged), "Accepted login attemps are detected"}' $Log

		awk -v user="$uname" 'BEGIN {failed=0} $10 == user {failed++} 
		END {print (failed), "Failed login attemps are detected"}' $Log
	fi
```

### Soal 2

a. membuat folder forensic_log_website_daffainfo_log
    
    `mkdir forensic_log_website_daffainfo_log`
    
b. mencari rata-rata IP request per jam.
    - Di sini, saya menghitung semua IP request, diambil dari jumlah barisnya (NR).
    - Lalu dibagi 12 (karena dalam file log website terdapat request hingga jam 12). maka, didapat rata-rata jumlah IP request per jam. ini menggunakan awk.
    - lalu, hasilnya dikirim ke file ratarata.txt. kalau sudah ada filenya, maka ditimpa supaya biar ga double2 filenya. (karna program ini bisa berkali2 dijalankan)
        
        ```bash
        awk 'BEGIN { FS= ":" }
        {  }
        END {
        totalJam = 12
        print "Rata-rata serangan adalah sebanyak ", NR/totalJam, " request per jam"}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt
        ```
        
hasil pada file ratarata.txt
`Rata-rata serangan adalah sebanyak  81,1667  request per jam`

c. menampilkan ip yang paling banyak.

- untuk mendapatkan ip, maka diambil kolom ke-1, dengan pemisah ":" per kolom. oleh karena itu, saya memakai FS = ":"
    
    ```bash
    awk 'BEGIN { 
    # untuk mendapatkan ip, maka diambil kolom ke-1, dengan pemisah ":" per kolom. oleh karena itu, saya memakai FS = ":”
    FS = ":"
    terbanyak = 0
    print ""
    }
    ```
    

- ip adress dimasukkan menjadi index array ip. lalu, setiap bertemu dengan suatu ip, maka nilai ipbertambah (di mana x adalah suatu ip). itu untuk menghitung jumlah berapa kali masing2 ip address muncul di sini.
    
    ```bash
    { 
    if(NR>1) {ip[$1]++}
    
    }
    ```
    

- di akhir, program me-loop-ing ip[i], untuk mencari berapa jumlah akses paling banyak dari suatu ip.
    
    ```bash
    END{
    	for (i in ip){
    #		print i, " has accessed ", ip[i], " times"
    		if(ip[i] > terbanyak){
    			terbanyak = ip[i]
    		}
    	}
    ```
    

- udah dapet jumlah maks nya berapa, lalu kita cari tau, ip berapa sih yang jumlah aksesnya segitu. dicari dengan cara looping. mungkin ini terlihat kurang efektif. tapi, codingan ini juga mengatasi kalau misalnya ternyata ada lebih dari 1 ip yang mempunyai jumlah akses tertinggi.
    
    ```bash
    printf "ip yang paling banyak mengakses server adalah:"
    	for (i in ip){
    		if( ip[i] == terbanyak){
    			printf "%s sebanyak %d requests", i, ip[i]
    		}
    	}
    print ""
    }' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt
    ```
    
    lalu, hasilnya ditaruh ke file result.txt. jika filenya sudah ada, maka isinya ditambahkan. karena kalau ditimpa, takutnya hasil dari codingan yang nomor b jadi kehapus)
    

d. mencari curl.

- menggunakan awk untuk mencari kata "curl".
- kemudian menghitung baris yg ada (pakai i++)
- print hasil
- hasilnya ditaruh ke file result.txt. (kalau file sudah ada, maka ditambah)

```bash
awk '/curl/ { # print $0
i++}
END{
print ""
print "Ada ", i, "request yang menggunakan curl sebagai user-agent"
}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```

e. mencari ada berapa ip yang mengakses di jam 2

- gunakan ":" sebagai pemisah kolom ->> (FS = ":")
    
    ```bash
    awk 'BEGIN { FS = ":"
    print "" }
    ```
    

- cari kalimat "22/Jan/2022:02:", karena itu jam 2
- lalu dihitung per ip nya, memanfaatkan array. (kayaknya ga penting sih ip nya dihitung)
    
    ```bash
    /22\/Jan\/2022:02:/{ ip[$1]++ }
    ```
    

- print ip nya yg mengakses di jam tersebut (yg sudah tersimpan pada array `ip` )
- hasilnya ditaruh ke file result.txt. (kalau file sudah ada, maka ditambah)
    
    ```bash
    END{
    	for (i in ip) print i
    }' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
    ```


hasil pada file result.txt :
```

ip yang paling banyak mengakses server adalah:"216.93.144.47" sebanyak 539 requests

Ada  58 request yang menggunakan curl sebagai user-agent

"145.239.154.84"
"88.80.186.144"
"2.228.115.154"
"109.237.103.38"
"128.14.133.58"
"194.126.224.140"

```

## Soal 3

a. `minute_log.sh`

Untuk memonitoring resource yang tersedia pada komputer tiap menit. untuk memonitoring memori, digunakan `free -m` dan didapat output seperti berikut.

```
              total        used        free      shared  buff/cache   available
Mem:           2982         737        1177           6        1067        2078
Swap:           448           0         448
```

Untuk menyimpan setiap resource memorinya, dapat dilakukan dengan mengabaikan kolom pertama dan baris pertama. Proses tersebut dapat dilakukan menggunakan awk sebagai berikut.

```
free -m | awk 'BEGIN{} NR!=1 {for(i=2;i<=NF;i++) printf $i",";}'
```
Lalu untuk menyimpan path dan sizenya, dapat dilakukan dengan awk sebagai berikut.
```
du -sh ~ | awk '{printf $2","$1}'
```
Setelah itu, resource-resource tersebut disimpan kedalam `metrics_{YmdHMS}.log` untuk data pada menit ke-M dan kedalam `temp_{YmdH}.log` untuk data per menit dalam jam ke-H. Untuk tanggal bisa diakses menggunakan code berikut.
```
dt=$(date +%Y%m%d%H%M%S)
dtagg=$(date +%Y%m%d%H)
```
Sebelum menyimpan setiap resource, file `temp_{YmdH}.log` dicek apakah ada atau tidak, jika ada, maka ubah permission filenya ke `write and read` karena jika filenya sudah ada, di akhir permission untuk setiap file log akan diubah ke `read-only`.

Untuk menjalankan program `minute_log.sh` setiap menit secara otomatis, dapat dilakukan menggunakan cron sebagai berikut.
```
* * * * * bash ~/minute_log.sh
```

b. `aggregate_minutes_to_hourly_log.sh`

Untuk agregasi resourcenya bisa didapatkan menggunakan awk ke `temp_{YmdH}.log` dengan Field Separator koma(,), lalu dari tiap kolom kecuali path, dicari nilai minimum, nilai maksimum, dan rata-ratanya. Lalu setiap agregasinya disimpan kedalam `metrics_agg_{YmdH}.log` dan permissionnya diubah ke `read-only`.

Untuk menjalankan program `aggregate_minutes_to_hourly_log.sh` setiap jam secara otomatis, dapat dilakukan menggunakan cron sebagai berikut.
```
0 * * * * bash ~/aggregate_minutes_to_hourly_log.sh
```
