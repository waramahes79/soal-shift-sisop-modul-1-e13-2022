mkdir -p ~/log

minimum=(1000000 1000000 1000000 1000000 1000000 1000000 1000000 1000000 1000000 1000000 1000000 1000000)
maximum=(0 0 0 0 0 0 0 0 0 0 0)
avg=(0 0 0 0 0 0 0 0 0 0 0)

dtagg=$(date --date='1 hour ago' +%Y%m%d%H)
path=$(du -sh ~ | awk '{printf $2","$1}')
awk 'BEGIN {FS=",";}
for(j=1;j<=11;j++) {
    avg[j-1]+=$j;
    if(minimum[j-1] > $j) minimum[j-1]=$j;
    if(maximum[j-1] < $j) maximum[j-1]=$j;
}
END{
    printf "minimum,"
    for(i=0;i<11;i++)  printf $minimum[i]",";
    echo $path  
    printf "maximum,"
    for(i=0;i<11;i++)  printf $maximum[i]",";
    echo $path  
    printf "average,"
    for(i=0;i<11;i++)  printf $(avg[i]/NR)",";
    echo $path  
}' temp_$dtagg.log >> ~log/metrics_agg_$dtagg.log

chmod 400 ~log/metrics_agg_$dtagg.log

