mkdir -p ~/log

ram=$(free -m | awk 'BEGIN{} NR!=1 {for(i=2;i<=NF;i++) printf $i",";}')
path=$(du -sh ~ | awk '{printf $2","$1}')
dt=$(date +%Y%m%d%H%M%S)
dtagg=$(date +%Y%m%d%H)

echo "$ram$path" > ~/log/metrics_$dt.log
if [[ -f  ~/log/temp_$dtagg.log ]]
then
    chmod 600 ~/log/temp_$dtagg.log
fi
cat  ~/log/metrics_$dt.log >>  ~/log/temp_$dtagg.log

chmod 400 ~/log/metrics_$dt.log
chmod 400 ~/log/temp_$dtagg.log

